import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import { Language } from 'angular-l10n';

@Component({
    selector: 'categories',
    template: require('./categories.component.html'),
    styles:   [require('./categories.component.scss')],
})
export class CategoriesComponent implements OnInit
{
    @Language()
    public lang:string;

    @Input()
    public myTitle:string;

    constructor()
    {
    }

    public ngOnInit():void
    {
    }

    public redirectToDevelopersTutorial():void
    {
        window.open('https://developers.plentymarkets.com/tutorials/angular-plugin', '_blank');
    }
}
